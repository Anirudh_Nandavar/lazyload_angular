import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events/events.component';

const routes: Routes = [
  { path: 'events', component: EventsComponent },
  {
    path: 'user',
    loadChildren: () =>
      import('./User/user-profile/user-profile.module').then(
        (m) => m.UserProfileModule
      ),
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
